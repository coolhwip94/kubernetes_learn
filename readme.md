# Kubernetes
> This repo hosts files associated with learning kubernetes

## Setup
---
> You can set up your cluster any way you want, this repo will use minikube

- For Linux
```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
```

- Starting your cluster
```
minikube start
```


## Commands
---
> Commands that may be helpful
- `kubectl get pod` : shows current running pods
```
kubectl get pods --watch

# extra info 
kubectl get pods -o wide


# pod info
kubectl describe pods hello-pod
```


## Pods
---
> Pods are the smallest unit for kubernetes, and consist of container or containers
- `Pods/pod.yml`: example of manifest for running a single pod
- `Pods/multi-pod.yml`: example of manifest for running multiple containers within a single pod

- Running or creating a pod
```
kubectl apply -f pod.yml
kubectl apply -f multi-pod.yml
```

- Deleting a pod
```
kubectl delete <pod_name>
# you can also reference the file used to create it
kubectl delete -f pod.yml
```

## Creating a service
---
> There are 3 types of services with kubernetes NodePort (external access via nodes), Cluster IP (internal cluster connectivity), Load Balancer (external access via cloud load balancer)


- Imperative
```
kubectl expose pod hello-pod --name=hello-svc --target-port=8080 --type=NodePort
```

- Declarative (Services/svc-nodeport.yml)
```
apiVersion: v1
kind: Service
metadata:
  name: ps-nodeport
spec:
  type: NodePort
  ports:
  - port: 80
    targetPort: 8080
    nodePort: 31111
    protocol: TCP
  selector:
    app: web
```

```
kubectl apply -f svc-nodeport.yml
```
```
kubectl get svc
```

- Exposing with minikube
```
minikube service hello-svc

# or forward using kubectl

kubectl port-forward service/hello-svc 7080:8080
```


## Creating Deployments

- `Deployments/deploy.yml`
```
apiVersion: apps/v1 <-------------- deployment spec
kind: Deployment
metadata:
  name: web-deploy
  labels:
    app: web  <-------- this label does not matter
spec: 
  replicas: 5
  selector:
    matchLabels:
      app: web <------------- this label needs to be matched below
  template: <----------------------------- pod spec
    metadata:
      labels:
        app: web  <------- this labeling needs to match above
    spec:
      terminationGracePeriodSeconds: 1
      containers:  <---------------------------------- container spec
      - name: hello-pod
        image: nigelpoulton/getting-started-k8s:1.0
        imagePullPolicy: Always
        ports:
        - containerPort: 8080

```

```
# deploy
kubectl apply -f deploy.yml

# view deployments
kubectl get deploy

# view replica sets
kubectl get rs

```
- Rolling updates
> This example shows settings that can be configured to perform rolling updates
```
...
spec:
  replicas: 10
  selector:
    matchLabels:
      app: web
  minReadySeconds: 5 <---- up and running for 5 seconds before cycling next one
  strategy: <--- settings for update method
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 0  <---- during the update at the most have 0 less than replicas number
      maxSurge: 1 <---- can have more than 1 in desired state during update

...

```
> monitor a deployment rollout
```
kubectl rollout status deploy <deployment_name>
```
> viewing rollout history
```
kubectl rollout history deploy <deployment_name>
```
> rolling back a deployment
```
kubectl rollout undo deploy <deployment_name> --to-revision <revision #>
```


## References
- https://minikube.sigs.k8s.io/docs/start/
- https://www.tecmint.com/install-kubernetes-cluster-on-centos-7/
- https://app.pluralsight.com/library/courses/kubernetes-getting-started/table-of-contents
